"use strict";

var path = require('path');
var rserve = require('rserve-js-v1');
var RAPI = require('./RAPI');

var callbackErrorFunction = "cbPluginError";


var tempwsio;
var curFileIndex;


function processRequest(wsio, data, config) {
    var dirname = module.parent.exports.dirname;
    tempwsio = wsio;
    var appid = data.app;
    var callbackfuncname;
    if(data.func !== undefined)
        callbackfuncname = data.func;
    else
        callbackfuncname = "pluginCallback";


    if (config.hasOwnProperty('pluginstate')) {
        var client = config.pluginstate.rserveclient;
        var RAPI = config.pluginstate.RAPI;
    }

    switch (data.query.command) {
        case "connect":
            if (config.hasOwnProperty('pluginstate') && config.pluginstate.hasOwnProperty('rserveclient')) {
                //closeServer(client, config);
                
            }

            connectServer(data.query.cmdparams, config, wsio, appid, callbackfuncname);
            break;
        case "close":
            console.log('going to close server');
            closeServer(config.pluginstate.rserveclient, config, callbackfuncname);
            break;
        case "info":
            getInfo(wsio, config.pluginstate.rserveclient, appid, callbackfuncname);
            break;

        case "loadFiles":
            console.log('loading files: ' + data.query.cmdparams.rparam[0].filename);
            curFileIndex = 0;
            config.pluginstate.RAPI.loadFilesToR(
                // rparam holds object list containig mapping from r-varname -> csv file
                data.query.cmdparams.rparam,
                // callback function which gets called at the end after all files have been read 
                function () {
                    console.log('finished loading files.. calling: ' + callbackfuncname);
                    module.parent.exports.broadcast('broadcast', { app: appid, func: callbackfuncname, data: { response: 'all fine' } });
                }.bind(this),
                // progress callback function which gets called after each loaded file
                function(response) {
                    console.log('progress callback with: ' + JSON.stringify(response) + ' calling: ' + data.query.progresscallback);
                    if(response.err !== undefined) 
                        module.parent.exports.broadcast('broadcast', { app: appid, func: callbackErrorFunction, data: { response: response.err } });
                    else
                        module.parent.exports.broadcast('broadcast', { app: appid, func: data.query.progresscallback, data: { response: response } });
                }.bind(this)
                );
            
            break;

        case "loadScript":
            var rscriptfile = data.query.cmdparams.rscriptfile;
            console.log('loading rscript file: ' + rscriptfile);

            loadRScript(RAPI, rscriptfile, function (response) {
                console.log('rscript file loaded: ' + rscriptfile);
                client.eval('get_sample_data()', function (err, response) {
                    console.log('get_sample_data answer: ' + response);
                })


                module.parent.exports.broadcast('broadcast', {
                    app: appid,
                    func: callbackfuncname,
                    data: {response: response}
                });

            });
            break;

        case "getData":
            console.log('getting data');
            console.log(JSON.stringify(data.query.cmdparams));
            var timeIdx = data.query.cmdparams.timeIdx;
            var runIdx = data.query.cmdparams.runIdx;
            //checkFirstArray(config.pluginstate.rserveclient);
            client.eval('create_plot_run_wavelength_time_json(data, ' + runIdx + ', ' + timeIdx + ');', function(err, response){
                console.log('eval: response');
                var entry = response[0];
                var retobj = JSON.parse(entry);
                module.parent.exports.broadcast('broadcast', { app: appid, func: callbackfuncname, data: { response: retobj } });
            });
                
            break;
    }



}

function getInfo(wsio, client, appid, callbackfuncname) {
    client.eval('R.Version()', function (err, response) {

        if (err) {
            module.parent.exports.broadcast('broadcast', { app: appid, func: cbPluginError, data: { error: err } });
        } else {
            module.parent.exports.broadcast('broadcast', { app: appid, func: callbackfuncname, data: { info: response } });
        }

    });
}

function connectServer(conndata, config, wsio, appid, callbackfuncname) {
    var me = this;
    console.log('connecting to: ' + JSON.stringify(conndata));
    var client = rserve.connect('tcp://' + conndata.host + ':' + conndata.port, function () {
        console.log('connected');
        console.log(arguments.length + ' ' + arguments[0]);
        if (client != null) {
            config.pluginstate = {};
            config.pluginstate.rserveclient = client;
            config.pluginstate.RAPI = new RAPI(client);

            module.parent.exports.broadcast('broadcast', {
                app: appid, func: callbackfuncname,
                data: { status: 'connected' }
            });


        };
    });
    client.on('error', function () {
        console.log('we have error');
        module.parent.exports.broadcast('broadcast', {
            app: me.appid, func: me.cbPluginError,
            data: { status: 'connection error' }
        });
    });

    client.on('timeout', function () {
        console.log('we have timeout');
    });

}

function loadRScript(RAPI, rscriptfile, callback) {
    console.log('loading loadRScript: ' + rscriptfile);

    RAPI.loadRScript(rscriptfile, callback);
}



function closeServer(client, config, callbackfuncname) {
    if (client != null) {
        console.log('closing connection to server');
        client.close();
     
    } else {
        console.log('no connection to server: not closing');
    }
    if(config.hasOwnProperty('pluginstate')){
        delete config.pluginstate.rserveclient;
        delete config.pluginstate;
        //console.log('removed rserve pluginstate' + config.pluginstate);
        //delete config.pluginstate;
    }
}



function checkFirstArray(client) {
    var rvarname = 'my2darray';
    client.eval(rvarname, function (err, result) {
        if (err)
            throw err;
        console.log('result ' + rvarname + ': ' + result.length);

        for (var i = 0; i < 5; i++)
            console.log(result[i][i]);

    });

}



module.exports = processRequest;