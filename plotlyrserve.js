
"use strict";
var plotlyrserve = SAGE2_App.extend({
    init : function(data) {
        this.SAGE2Init("div", data);

		
        this.rserverhost = data.state.rserverhost;
        this.rserverport = data.state.rserverport;

        this.isRserveCallInProgress = false;
        this.currentActiveRserveCallBack = [];

        this.resizeEvents = "onfinish";

		this.buttonstatus = 0;
        // initial time index, which should perform well
        this.plotTimeIdx = 20;
        this.selectedRun = 1;

        this.initUI();

        this.statuselem.innerText = "Waiting for Server connection";

		this.sage2mouseevent = new SAGE2_MouseEvent(this);
		
        this.connectRserve();

        // this var hols the plot
        this.plotlyPlot;
    },

    initUI : function () {
        this.element.style.background = "white";

        this.uicontrols  = document.createElement("div");
        this.uicontrols.style.width = "250px";
        this.uicontrols.style.height = "100%";
        this.uicontrols.style.background = "#dddddd";
        this.uicontrols.style.float = "left";

        this.uiplot = document.createElement("div");
        this.uiplot.style.width = "100%";
        this.uiplot.style.height = "100%";
        this.uiplot.style.background = "#aaaaaa";
        

        var statuslabel = document.createElement("div");
        statuslabel.style.margin = "5px 5px 5px 5px";
        statuslabel.innerText = "RServe Connection status" + (isMaster==true ? " is master" : " is slave");
        this.statuselem = document.createElement("div");
        this.statuselem.style.margin = "5px 5px 5px 5px";
        

        this.uicontrols.appendChild(statuslabel);
        this.uicontrols.appendChild(this.statuselem);

        this.statuselem.addEventListener('mousemove', function() {
            console.log('yadaa');
        }.bind(this), false);

        this.statuselem.addEventListener('mouseup', function() {
            this.getInfo();

        }.bind(this), false);


		var loadbutton = BootstrapUIHelper.createButton(
			"Load Data", 
			function(){
				console.log('loadbutton click');
                loadbutton.innerText = 'Loading..';
				this.loadFiles(function() {
					loadbutton.style.background = "#eeddee";
                    loadbutton.innerText = 'Loading Done';
				});
			}.bind(this)
		);
			
		this.uicontrols.appendChild(loadbutton);
		
			
		var getdatabutton = BootstrapUIHelper.createButton(
			"Show Data", 
			function(){
				console.log('getdatabutton click');
				this.getData(function() {
					getdatabutton.style.background = "#eeddee";
				});
			}.bind(this)
		);
			
		this.uicontrols.appendChild(getdatabutton);

        this.progressbar = BootstrapUIHelper.progressBar();
        this.uicontrols.appendChild(this.progressbar);
        //this.progressbar.setProgress(40);
		
        var runchoosermenu = BootstrapUIHelper.createMenu(
            "Select Run",
            [
                {
                    label:"Run 1",
                    callback : function() { 
                            this.selectedRun = 1;
                            this.selectedMenuEntry.innerText = "Run " + this.selectedRun + " selected";
                    }.bind(this)
                },
                {
                    label:"Run 2",
                    callback : function() { 
                            this.selectedRun = 2;
                            this.selectedMenuEntry.innerText = "Run " + this.selectedRun + " selected";
                    }.bind(this)
                },
                {
                    label:"Run 3",
                    callback : function() { 
                            this.selectedRun = 3;
                            this.selectedMenuEntry.innerText = "Run " + this.selectedRun + " selected";
                    }.bind(this)
                },                {
                    label:"Run 4",
                    callback : function() { 
                            this.selectedRun = 4;
                            this.selectedMenuEntry.innerText = "Run " + this.selectedRun + " selected";
                    }.bind(this)
                },                {
                    label:"Run 5",
                    callback : function() { 
                            this.selectedRun = 5;
                            this.selectedMenuEntry.innerText = "Run " + this.selectedRun + " selected";
                    }.bind(this)
                },           ]
        );
        this.uicontrols.appendChild(runchoosermenu);
        this.selectedMenuEntry = document.createElement("div");
        this.selectedMenuEntry.style.margin = "5px 5px 5px 5px";
        this.selectedMenuEntry.innerText = "Run " + this.selectedRun + " selected";
        this.uicontrols.appendChild(this.selectedMenuEntry);
        


        this.controls.addSlider({
            identifier: "sliderTimeIdx",
            minimum: 1,
            maximum: 20,
            property: "this.sliderPlotTimeIdx",
            step: 1
        });
		
		
		this.controls.addButton({ label: "Rotate", identifier: "rotate", position: 4 });
		this.controls.addButton({ label: "Pan", identifier: "pan", position: 8 });
		this.controls.addButton({ label: "Reset View", identifier: "resetview", position: 12 });
		
		
		// switch for rotate / pan control
		this.plotcontroltype = 1;
				
        this.controls.finishedAddingControls();
        this.enableControls = true;

        this.element.appendChild(this.uicontrols);
        this.element.appendChild(this.uiplot);
	},


    quit: function() {
        
    },

    callPlugin : function(rpcparam, callbacks) {
        this.isRserveCallInProgress = true;
        this.currentActiveRserveCallBack = [];
        if(arguments.length > 1)
            for(var i = 1; i < arguments.length; i++)
                if(arguments[i] !== undefined)
                    this.currentActiveRserveCallBack.push(arguments[i]);

        if (isMaster)
            this.applicationRPC(rpcparam, "pluginCallback", true);
    },

    /**
     * Main callback function that gets called from the plugin
     * This function will then execute all the listeners that have been given as
     * parameters when calling 'callPlugin' 
     */
    pluginCallback : function (data) {
        this.isRserveCallInProgress = false;
        var callbacks = this.currentActiveRserveCallBack; // save reference (concurrency)
        if (callbacks.length > 0)
            for(var i = 0; i < callbacks.length; i++) {
                callbacks[i].bind(this)(data);
            }

    },
    cbPluginError : function(data) {

        this.statuselem.innerText = 'Error';
    },

    connectRserve : function(cb) {

        this.callPlugin({command:'connect', cmdparams:{host:this.rserverhost, port : this.rserverport}}, this.cbPluginConnSucc, cb);
    },
    cbPluginConnSucc : function (data) {

        this.statuselem.innerText = data.status;

    },
    
    
    getInfo : function(cb) {

        this.callPlugin({command:'info'}, this.cbPluginInfo, cb);
    },
    cbPluginInfo : function(data) {

        console.log('cbPluginInfo');
        this.statuselem.innerText = data.info;
    },


    loadFiles : function( cb) {
        var files = [];
		files.push({varname:'data1', filename: 'public/' + this.resrcPath.replace(/http.*?:\/\/.*?\//, '') + '/data/run1v2.csv'});
        
		files.push({varname:'data2', filename: 'public/' + this.resrcPath.replace(/http.*?:\/\/.*?\//, '') + '/data/run2v2.csv'});
		files.push({varname:'data3', filename: 'public/' + this.resrcPath.replace(/http.*?:\/\/.*?\//, '') + '/data/run3v2.csv'});
		files.push({varname:'data4', filename: 'public/' + this.resrcPath.replace(/http.*?:\/\/.*?\//, '') + '/data/run4v2.csv'});
		files.push({varname:'data5', filename: 'public/' + this.resrcPath.replace(/http.*?:\/\/.*?\//, '') + '/data/run5v2.csv'});
        
        this.totalnumfiles = files.length;
        this.curprogressfileidx = 0;

		console.log('calling plugin');
        this.callPlugin({command:'loadFiles', cmdparams : {rparam : files}, progresscallback:'cbPluginLoadFilesProgress'}, this.cbPluginLoadFilesDone, cb);
    },
    cbPluginLoadFilesDone : function(data) {

        console.log('csv files loaded.. loading script');
        this.loadScript(this.cbPluginLoadScriptDone);

    },
    cbPluginLoadFilesProgress : function(data) {

        console.log('progress: csv files loaded.. ');
        this.curprogressfileidx++;
        this.progressbar.setProgress(100 * this.curprogressfileidx / this.totalnumfiles);
        //this.loadScript(this.cbPluginLoadScriptDone);

    },

    loadScript : function(cb) {
        var rfile = 'public/' + this.resrcPath.replace(/http.*?:\/\/.*?\//, '') + '/rscripts/bio_chemist.R';
        this.callPlugin({command:'loadScript', cmdparams : {rscriptfile : rfile}}, this.cbPluginLoadScriptDone, cb);
    },
    cbPluginLoadScriptDone : function(data) {
        this.statuselem.innerText = "data and script loaded";
    },


	getData : function(cb) {
		this.callPlugin({command:'getData', cmdparams : {timeIdx : this.plotTimeIdx, runIdx : this.selectedRun}}, this.cbPluginRecvData, cb);
	},
    cbPluginRecvData : function(data) {
        console.log('cbPluginRecvData: ');

        console.log('data size: ' + data.response.data[0].x.length);
        if(this.plotlyPlot == undefined) {
            this.plotlyPlot = document.createElement('div');
            this.plotlyPlot.style.position = "relative";
            this.plotlyPlot.style.left = this.uicontrols.clientWidth + "px";
            this.plotlyPlot.style.width = this.element.clientWidth - this.uicontrols.clientWidth + "px";
            this.plotlyPlot.style.height = "100%";
            this.uiplot.appendChild(this.plotlyPlot);


        } else {
        }
        Plotly.newPlot(this.plotlyPlot, data.response.data, data.response.layout);            


    },
    
    sendData : function() {

    },

    draw: function(date) {
		
    },

	quit: function() {
		
		if(this.plotlyPlot !== undefined) {
			var fulllayout = this.plotlyPlot._fullLayout;
			
			var _scene = this.plotlyPlot._fullLayout.scene._scene;
			if(_scene.glplot !== undefined)
				_scene.glplot.dispose();
		}
	},
	
    resize: function(date) {

        if(this.plotlyPlot !== undefined) {
			var plotSize = {
				width : this.element.style.width - this.uicontrols.style.width,
				height : this.plotlyPlot.clientHeight,
                autosize: true
			};
            this.plotlyPlot.style.width = this.element.clientWidth - this.uicontrols.clientWidth + "px";
			Plotly.relayout(this.plotlyPlot, plotSize);
        }
    },

    setPlotCamera : function(data, remote) {
        if(!isMaster || (remote !== undefined && remote == true)){
			if(this.plotlyPlot !== undefined ){
				//console.log('setPlotCamera: slave setting camera ' + JSON.stringify(data.cameraObj.eye));
				var _scene = this.plotlyPlot._fullLayout.scene._scene;
				_scene.setCamera(data.cameraObj);
			}
        }  
    },
	event: function(eventType, position, user_id, data, date) {
        var type;
		var changed = false;
		if(this.plotlyPlot !== undefined) {
			var _scene = this.plotlyPlot._fullLayout.scene._scene;
			var cam = _scene.camera;
		}
		/**
		 * Handle UI input
		 */
		if(eventType == "widgetEvent") {

			switch(data.identifier) {
				case "rotate":
					this.plotcontroltype = 1;

					break;
				case "pan":
					this.plotcontroltype = 2;

					break;
				case "resetview":
					
					_scene.setCameraToDefault();
					break;
				case "sliderTimeIdx": {
					switch (data.action) {
						case "sliderLock":
							break;
						case "sliderUpdate":
							break;
						case "sliderRelease":
							this.plotTimeIdx = this.sliderPlotTimeIdx;
							this.statuselem.innerText = "Plot time index: " + this.plotTimeIdx;
							break;
						default:
							console.log("No handler for: " + data.identifier + "->" + data.action);
							break;
					}
					break;
				}
				
			}

			return;
		}

        if(eventType == "pointerMove") {
			
			if(this.buttonstatus == 1 ){
				if(isMaster && this.plotlyPlot !== undefined){
		
					//console.log('master is rotating plot');
					this.lastTime = performance.now();
					//this.lastTime += 10;
					
					//console.log('lastTime: ' + this.lastTime + ', dx / dy : ' + data.dx + ' ' + data.dy);
					if(this.plotcontroltype == 1)
						cam.view.rotate(this.lastTime, -0.001 * data.dx, 0.001 * data.dy, 0.0);
					else if (this.plotcontroltype == 2) {
						cam.view.pan(this.lastTime, -0.001 * data.dx * cam.distance, 0.001 * data.dy * cam.distance, 0.0);
					}
					changed = true;
				} else {
					return;
				}
				
			}
        }
        if(eventType == "pointerPress") {
            this.buttonstatus = 1;

        }
        if(eventType == "pointerRelease") {
            this.buttonstatus = 0;

        }
		if(eventType === "pointerScroll") {
			if(isMaster && this.plotlyPlot !== undefined){
				this.lastTime = performance.now();
				cam.view.pan(this.lastTime, 0, 0, 0.001 * data.wheelDelta);
				changed = true;
			} else {
				return;
			}
		}

		if(changed) {
			var cameraObj = _scene.getCamera();
			//console.log('master sending broadcast ' + JSON.stringify(cameraObj.eye));
			this.state.cameraObj = cameraObj;
			this.refresh();
			this.broadcast('setPlotCamera', {cameraObj:cameraObj});
			
			return;
		}		
		
		this.sage2mouseevent.handleEvent(eventType, position, user_id, data, date);
    },
    
});