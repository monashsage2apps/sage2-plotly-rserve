var BootstrapUIHelper = {};

BootstrapUIHelper.createMenu = function (label, entries) {
    // create drop down menu container
    // everything inside belongs to this drop down menu
    var dropdownmenu = document.createElement('div');
    $(dropdownmenu).addClass('dropdown');

    // create button, that opens drop down menu
    var dropdownbutton = document.createElement('button');
    $(dropdownbutton).addClass('btn');
    $(dropdownbutton).addClass('btn-primary');
    $(dropdownbutton).addClass('dropdown-toggle');
    $(dropdownbutton).attr('data-toggle', 'dropdown');
    dropdownbutton.innerHTML = label + '<span class="caret"></span>';

    dropdownmenu.appendChild(dropdownbutton);

    // create list, that holds menu items
    var entrylist = document.createElement('ul');
    $(entrylist).addClass('dropdown-menu');

    for (var id in entries) {
        var entry = entries[id];
        var menuentry = document.createElement('li');

        var entrylink = document.createElement('a');
        entrylink.onclick = entry.callback;
        entrylink.innerText = entry.label;
        menuentry.appendChild(entrylink);
        entrylist.appendChild(menuentry);

    }


    dropdownmenu.appendChild(entrylist);




    return dropdownmenu;
}

BootstrapUIHelper.createButton = function (label, callback) {
    // create button, that opens drop down menu
    var button = document.createElement('button');
    $(button).addClass('btn');
    $(button).addClass('btn-default');
    

    button.innerHTML = label;
    button.clickcallback = callback;

    button.addEventListener('click', function (me) {
        if (!$(me.currentTarget).hasClass('disabled')) {
            button.clickcallback(me);
        }
    });


    button.onmousedown = function (me) {
        if (!$(me.currentTarget).hasClass('disabled')) {
            me.currentTarget.oldstyle = {};
            me.currentTarget.oldstyle.borderColor = me.currentTarget.style.borderColor;
            me.currentTarget.style.borderColor = "#192";
        }
    }
    button.onmouseup = function (me) {
        if (!$(me.currentTarget).hasClass('disabled')) {
            if (me.currentTarget.oldstyle.borderColor !== undefined)
                me.currentTarget.style.borderColor = me.currentTarget.oldstyle.borderColor;
        }

    }

    return button;
}

BootstrapUIHelper.setButtonEnabled = function (buttonelement, enabled) {
    if(enabled) {
        $(buttonelement).removeClass('disabled');
    } else {
        $(buttonelement).addClass('disabled');    
    }
    
}

BootstrapUIHelper.progressBar = function() {
    var progressbar = document.createElement('div');
    $(progressbar).addClass('progress');
    var bar = document.createElement('div');
    $(bar).addClass('progress-bar');
    $(bar).attr('role',  'progressbar')
    $(bar).attr('style',  'width:0%');

    progressbar.appendChild(bar);

    var progresstext = document.createElement('span');
    $(progresstext).addClass('sr-only');

    bar.appendChild(progresstext);

    progressbar.setProgress = function(progress) {
        var elem = $(this).children('div');
        $(this).children('div')[0].style.width = progress + '%';
        var span = $(this).find('span');
        span[0].innerText = progress + '%';
    }

    return progressbar;
}