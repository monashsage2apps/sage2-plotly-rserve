"use strict";

var Type = require('type-of-is');
var rserve = require('rserve-js-v1');

var csv = require('csv-parser')
var fs = require('fs')

var async = require('async');


class RAPI {

	

	constructor(client) {
	
		this.client = client;
		this._ = rserve.const;

	}

	/**
	 * Loads a csv file (2d array) to the rserve instance 
	 * and assigns the given variable name to it. WHen the operation
	 * is finished, the callback function is called
	 * @Param: files is an array of objects containting to fields.
	 * 			varname: is the resulting R variable name
	 * 			filename: is the path to the CSV file
	 * @Param: callback: a function being called after ALL files have been read
	 * using an array of parameter objects like: 
	 *	[
	 * 		{varname:'my2darray', filename: 'pathToCSVFile'}
	 *	]
	 */	
	loadFilesToR(files, callback, progresscallback) {
		this.progresscallback = progresscallback;
		console.log('loadFilesToR with files ' + files);
		async.eachSeries(
			files,
			this._loadCSVFileToR.bind(this),
			callback
			);
	}

	loadRScript(rscriptfile, callback) {
		console.log('RAPI: loadRScript: ' + rscriptfile);
		var me = this;
		fs.readFile(rscriptfile, "utf-8", function(err, data) {
			if(err)
				throw err;
			me._sendRFileDataToR(data, callback);
		});
		
	}
	


	/****************************************************
	INTERNAL METHODS
	*****************************************************/
	
	/**
	 * Loads a csv file (2d array) to the rserve instance 
	 * and assigns the given variable name to it
	 * using params object {varname:'my2darray', filename: 'pathToCSVFile'}
	 */
	_loadCSVFileToR(params, asynccallback) {
		console.log('here');
		var rvarname = params.varname;
		var file = params.filename;
		var me = this;
		
		console.log('reading csv file');

		var numlines = 1;

		/*
			First read the file to figure out the number of rows
			This info is later used to calculate the array space
		*/

		fs.createReadStream(file)
		.on('data', function(chunk) {
			for(var i = 0; i < chunk.length; i++)
				if(chunk[i] == 10) //'\n'
					numlines++;
		})
		.on('end', function(){
			console.log('file has #lines: ' + numlines);
			me._readCSVFileContent(rvarname, file, numlines, asynccallback);
			
		});


		
	}

	_readCSVFileContent(rvarname, file, vdim, asynccallback) {

		var dataarray;
		var curline = 0;
		var arrayIdx = 0;
		var hdim;
		var me = this;
	
		fs.createReadStream(file)
			.pipe(csv())
			.on('headers', function(headers) {
				hdim = headers.length;
				console.log('reading header with #columns: ' + hdim);
				dataarray = new Array(hdim * vdim);
				console.log('reading...');
				for(var i = 0; arrayIdx < hdim; arrayIdx++, i++)
					dataarray[vdim * arrayIdx] = headers[i];
				curline++;
			})
			.on('data', function(data) {
				//console.log('reading data');
				arrayIdx = 0;
				for(var key in data) {
					dataarray[curline + vdim * arrayIdx++] = data[key];
				}
				curline++;
			})
			.on('end', function() {
				console.log('having array with size: ' + dataarray.length);
				
				me._sendArrayToRserve(rvarname, dataarray, hdim, vdim, asynccallback);
			});

	}

	_sendArrayToRserve(rvarname, dataarray, hdim, vdim, asynccallback) {
		console.log('assignSEXP size array: ' + dataarray.length + ' dim h v: ' + hdim + ' ' + vdim);
		var me = this;

		this.client.assignSEXP(
			rvarname,
			{
				type: this._.XT_ARRAY_DOUBLE | this._.XT_HAS_ATTR,
				value: dataarray,
				attr: {
					type : this._.XT_LIST_TAG,
					value : [
						{
							type : this._.XT_ARRAY_INT,
							value : [vdim, hdim]
						},
						{
							type : this._.XT_SYMNAME,
							value : "dim"
						}
					]
					
				}
			},
			function(err) {
				if(err !== null) {
					console.log(' we have error : ' + err);
					throw err;
				}

				console.log(' we have sent array to R with varname: ' + rvarname);
				
				console.log('calling asynccallback');
				me.progresscallback(rvarname);
				asynccallback();
			}
		);
/*
		this.client.assignSEXP(
			rvarname,
			{
				type: this._.XT_ARRAY_DOUBLE,
				value: dataarray
				
			},
			function(err) {
				if(err !== null)
					console.log(' we have error : ' + err);

				console.log(' we have sent array to R with varname: ' + rvarname);
				
				console.log('calling asynccallback');

				asynccallback();
			}
		);
*/
	}

	_sendRFileDataToR(rfileData, callback) {
		console.log('sending rfiledata to R');
		//console.log(rfileData);
		this.client.eval(rfileData, function(err, response){
			console.log('call from eval:');
			if(err) {
				console.log(err);
			}
			console.log('rfiledata successfully evaluated');
			//console.log('response: ' + response);

			callback();
			console.log('after calling callback in _sendRFileDataToR');
		})
	}

}

module.exports = RAPI;